package com.bb.ws;

import javax.jws.WebService;

@WebService
public class SampleService {

	public String getService(String serviceName){
		return "service : "+serviceName;
	}
}
