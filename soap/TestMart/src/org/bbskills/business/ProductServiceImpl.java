/**
 * 
 */
package org.bbskills.business;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ProductServiceImpl {
	
	List<String> bookList = new ArrayList<String>();
	
	List<String> gameList = new ArrayList<String>();
	
	List<String> clothList = new ArrayList<String>();
	
	
	public ProductServiceImpl() {
		bookList.add("Ocean In A Tea Cup");
		bookList.add("Cracking Coding Interview");
		bookList.add("Leader Who has No title");
		
		gameList.add("Counter Strike");
		gameList.add("Winter Soilder");
		gameList.add("Angry Birds");
		
		clothList.add("Jeans");
		clothList.add("T-Shirts");
		clothList.add("Kurtas");
	}
	

	public List<String> getProducts(String category) {
		category = category.toLowerCase();
		if (category.equals("books")) {
			return bookList;
		} else if (category.equals("games")) {
			return gameList;
		} else if(category.equals("clothes")){
			return clothList;
		}
		return null;
	}
}
