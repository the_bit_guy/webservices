/**
 * 
 */
package org.bbskills.core;

import java.util.Arrays;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.bbskills.business.ProductServiceImpl;

/**
 * @author Bhabadyuti Bal
 *
 */
@WebService(name="TestMartCatalog", portName="TestMartCatalogPort", serviceName="TestMartCatalogService",
			targetNamespace="http://www.testmart.com")
public class ProductCatalog {
	
	
	ProductServiceImpl productServiceImpl = new ProductServiceImpl();
	
	@WebMethod(action="fetch_categories", operationName="fetchCategories")
	public List<String> getProductCategories(){
		return Arrays.asList(new String[] {"Books", "Games", "Clothes"});
	}
	
	@WebMethod(exclude=true)
	public List<String> getProducts(final String category) {
		return productServiceImpl.getProducts(category);
	}

}
